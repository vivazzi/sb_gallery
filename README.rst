=======
sb_gallery
=======

sb_gallery is django-cms plugin which is part of SBL (Service bussiness library, https://vits.pro/dev/).

This plugin adds image gallery to page.


Installation
============

sb_gallery requires sb_core package: https://bitbucket.org/vivazzi/sb_core/

There is no sb_gallery in PyPI, so you can install this package from bitbucket repository only.

::
 
     $ pip install hg+https://bitbucket.org/vivazzi/sb_gallery


Configuration 
=============

1. Add "sb_gallery" to INSTALLED_APPS after "sb_core" ::

    INSTALLED_APPS = (
        ...
        'sb_core',
        'sb_gallery',
        ...
    )

2. Run `python manage.py migrate` to create the sb_gallery models.  
