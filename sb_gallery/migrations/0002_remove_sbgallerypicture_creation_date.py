from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_gallery', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sbgallerypicture',
            name='creation_date',
        ),
    ]
