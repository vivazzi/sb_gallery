from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_gallery', '0007_auto_20160809_1301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbgallery',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='sb_gallery_sbgallery', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE),
        ),
    ]
