from django.db import models, migrations
import sb_core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sb_gallery', '0006_auto_20160603_2259'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbgallery',
            name='border_color',
            field=sb_core.fields.ColorField(default='#fff', help_text='\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430<br/><a rel="nofollow" target="_blank" href="http://getcolor.ru/">\u0421\u0435\u0440\u0432\u0438\u0441 \u043f\u043e \u043e\u043f\u0440\u0435\u0434\u0435\u043b\u0435\u043d\u0438\u044e \u0446\u0432\u0435\u0442\u0430</a>', max_length=30, verbose_name='\u0426\u0432\u0435\u0442 \u0433\u0440\u0430\u043d\u0438\u0446', blank=True),
        ),
        migrations.AlterField(
            model_name='sbgallerypicture',
            name='description',
            field=models.TextField(default='', verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sbgallerypicture',
            name='folder',
            field=models.CharField(default='', max_length=5, editable=False, blank=True),
            preserve_default=False,
        ),
    ]
