from django.apps import AppConfig

from django.utils.translation import gettext_lazy as _
from django.db.models.signals import pre_save


class SBGalleryConfig(AppConfig):
    name = 'sb_gallery'
    verbose_name = _('Gallery')

    def ready(self):
        from sb_gallery.signals import pre_save_gallery_handler
        from sb_gallery.models import SBGallery

        pre_save.connect(pre_save_gallery_handler, sender=SBGallery)
