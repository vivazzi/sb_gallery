from django import forms

from sb_core.mixins import TemplateFormMixin
from sb_gallery.models import SBGallery, SBGalleryPicture


class SBGalleryForm(TemplateFormMixin, forms.ModelForm):
    class Meta:
        exclude = ()
        model = SBGallery

    class Media:
        js = ['sb_core/js/sb_library.js', 'sb_gallery/sb_gallery_admin.js', ]


class SBGalleryPictureForm(forms.ModelForm):
    class Meta:
        exclude = ()
        model = SBGalleryPicture

        widgets = {
            'description': forms.Textarea(attrs={'cols': 50, 'rows': 3}),
        }
