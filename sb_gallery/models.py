from os.path import basename

from cms.models import CMSPlugin
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from sb_core.constants import color_ht, pic_ht, title_ht, shadow_ht
from sb_core.fields import PicField, ColorField
from sb_core.folder_mechanism import upload_to_handler
from sb_core.models import PicModel, copy_relations_fk
from sb_core.fields import TemplateField
from sb_core.template_pool import template_pool


class SBGallery(CMSPlugin):
    title = models.CharField('Название', max_length=255, blank=True, null=True, help_text=title_ht)

    is_full_display = models.BooleanField('Открывать изображения на весь экран при щелчке?', default=True)

    # thumbnails options
    width = models.PositiveIntegerField('Ширина, пиксели', default=225)
    height = models.PositiveIntegerField('Высота, пиксели', default=150)

    count_in_row = models.PositiveIntegerField(
        'Количество миниатюр в ряду', null=True, blank=True,
        help_text='Размещает определённое количество миниатюр в ряду. При использовании данного поля убедитесь, '
                  'что значения Ширины и Высоты миниатюр указаны с запасом, так как изображения расстягиваются '
                  'по ширине колонки. '
                  'При этом миниатюры с малыми размерами могут выглядеть некачественно.<br/>'
                  'Подробнее на странице: <a href="http://vits.pro/info/components/gallery/">Настройка галереи</a>'
    )
    count_in_row_sm = models.PositiveIntegerField(
        'Количество миниатюр в ряду на мобильных экранах', null=True, blank=True,
        help_text='Работает, если заполнено поле "Количество миниатюр в ряду"'
    )

    is_shadow = models.BooleanField('Добавить тень?', default=True)
    shadow_pars = models.CharField('Параметры тени', max_length=100, blank=True, null=True,
                                   default='0 0 10px 0 rgba(0,0,0,0.5)', help_text=shadow_ht)

    is_border = models.BooleanField('Добавить границу?', default=True)
    border_size = models.PositiveSmallIntegerField('Размер границ, пикс', default=3, null=True, blank=True,
                                                   validators=[MinValueValidator(1), ])
    border_color = ColorField('Цвет границ', blank=True, default='#fff', help_text=color_ht)

    template = TemplateField(_('Template'), blank=True)

    def copy_relations(self, old_instance):
        copy_relations_fk(self, old_instance, 'gallery')

    @property
    def style(self):
        border = 'border: {}px solid {};'.format(self.border_size, self.border_color) if self.is_border else ''
        shadow = 'box-shadow: {};'.format(self.shadow_pars) if self.is_shadow else ''
        return ''.join([border, shadow])

    def clean(self):
        if self.is_shadow and not self.shadow_pars:
            raise ValidationError('При использовании тени укажите значение в поле "Параметры тени"')

        if self.is_border and not (self.border_size and self.border_color):
            raise ValidationError('При использовании границы укажите значения в обоих полях: "Размер границ", '
                                  '"Цвет границ"')

    def __str__(self):
        res = self.title or ''

        if self.template:
            res += template_pool.obj_template_str(self, res)

        return res

    class Meta:
        db_table = 'sb_gallery'
        verbose_name = 'Галерея'
        verbose_name_plural = 'Галереи'


class SBGalleryPicture(PicModel):
    gallery = models.ForeignKey(SBGallery, verbose_name='Галерея', related_name='pics', on_delete=models.CASCADE)

    pic = PicField('Картинка', upload_to=upload_to_handler, help_text=pic_ht)
    description = models.TextField('Описание', blank=True)

    order = models.PositiveIntegerField('Порядок', db_index=True)

    def __str__(self):
        return basename(self.pic.name)

    class Meta:
        ordering = ('order', )
        db_table = 'sb_gallery_picture'
        verbose_name = 'Картинка в галерее'
        verbose_name_plural = 'Картинки в галерее'
