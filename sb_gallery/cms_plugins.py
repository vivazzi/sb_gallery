from adminsortable2.admin import SortableInlineAdminMixin
from django.contrib import admin
from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

from sb_core.mixins import RenderTemplateMixin
from sb_core.constants import BASE
from sb_gallery.forms import SBGalleryForm, SBGalleryPictureForm

from sb_gallery.models import SBGallery, SBGalleryPicture


class SBGalleryPictureInline(SortableInlineAdminMixin, admin.TabularInline):
    model = SBGalleryPicture
    form = SBGalleryPictureForm
    extra = 0


class SBGalleryPlugin(RenderTemplateMixin, CMSPluginBase):
    module = BASE
    model = SBGallery
    name = 'Галерея'
    render_template = 'sb_gallery/sb_gallery.html'
    form = SBGalleryForm

    inlines = (SBGalleryPictureInline, )

    fieldsets = (
        ('', {
            'fields': ('title', 'is_full_display', 'template')
        }),
        ('Внешний вид миниатюр', {
            'fields': (('width', 'height'), ('count_in_row', 'count_in_row_sm'),
                       ('is_border', 'border_size', 'border_color'),
                       ('is_shadow', 'shadow_pars'))
        })
    )


plugin_pool.register_plugin(SBGalleryPlugin)
