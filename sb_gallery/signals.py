from sb_core.utils.thumbnail import delete_thumbs


def pre_save_gallery_handler(sender, instance, **kwargs):
    if instance.id:
        try:
            obj = sender.objects.get(id=instance.id)
            if instance.width != obj.width or instance.height != obj.height:
                [delete_thumbs(pic.pic) for pic in instance.pics.all()]
        except sender.DoesNotExist:
            pass
